<?php
    include 'header.php';
    session_start();
    if( isset($_SESSION["userid"]) && isset($_SESSION["type"])){
        if($_SESSION["type"] == "profesor"){
            header('Location: homeProfesor.php');
        }
        elseif($_SESSION["type"] == "alumno"){
            header('Location: homeAlumno.php');
        }
        else{       //no puede haber otro tipo de sesion
            session_destroy();
        }
    }
    $msg = isset($_GET["msg"]) ? $_GET["msg"] : "welcome";
?>

<link href="css/signin.css" rel="stylesheet">
<body class="text-center">
    <?php
        if ($msg == "baddb") echo "<div class=\"alert alert-warning\">Error al conectarse a la base de datos</div>";
        elseif ($msg == "baduser") echo "<div class=\"alert alert-danger\">Credenciales de inicio de sesión Inválidas</div>";
        elseif ($msg == "welcome") echo "<div class=\"alert alert-primary\">Bienvenid@, inicie sesión para continuar</div>";
        elseif ($msg == "regerr") echo "<div class=\"alert alert-danger\">Error al realizar el Registro</div>";
        elseif ($msg == "regsucc") echo "<div class=\"alert alert-success\">Alumno registrado con éxito</div>";
    ?>
    <div class="mock-body">
        <form class="form-signin" method="post" action="signInController/signInAlumno.php">
            <h1 class="h3 mb-3 font-weight-normal">Iniciar Sesión Alumno(a)</h1>
            <input type="text" name="alumnoId" id="alumnoId" class="form-control" placeholder="Rol Alumno" required autofocus>
            <input type="password" name="alumnoPw" id="alumnoPw" class="form-control" placeholder="Contraseña" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <hr>
            <a href="registerAlumno.php">Registrar Alumno</a>
        </form>
        <form class="form-signin" method="post" action="signInController/signInProfesor.php">
            <h1 class="h3 mb-3 font-weight-normal">Iniciar Sesión Profesor(a)</h1>
            <input type="number" name="profesorId" id="profesorId" class="form-control" placeholder="Id Profesor" required autofocus>
            <input type="password" name="profesorPw" id="profesorPw" class="form-control" placeholder="Contraseña" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>
    </div>
</body>