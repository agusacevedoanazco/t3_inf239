<?php
include 'profesorNavBar.php';
include 'dbconfig_og.php';

$query = 'select rolalumno, nombre, apellido, anoingreso from alumno;';

$result = pg_query($dbconn,$query);
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">

    <?php if (!$result){
        echo "<div class=\"alert alert-warning\">Error al comunicarse con la base de datos</div>";
        pg_close($dbconn);
    }
    else{
    ?>

    <div class="jumbotron text-center">
        <h1>Listado de Alumnos</h1>
    </div>
    <table class="table table-bordered">
        <thead class="thead-dark">
            <th>Rol Alumno</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Año de Ingreso</th>
            <th>Editar</th>
        </thead>
        <?php
            echo "<tbody>";
            while ($alumno = pg_fetch_row($result)){
                echo "<tr>";
                echo "<td>".$alumno[0]."</td>";
                echo "<td>".$alumno[1]."</td>";
                echo "<td>".$alumno[2]."</td>";
                echo "<td>".$alumno[3]."</td>";
                echo "<td align=\"center\"><a href=\"profesorUpdateAlumno.php?id=".$alumno[0]."\">Editar Alumno</td>";
                echo "</tr>";
            }
            echo "</tbody>";
            pg_close($dbconn);
        }
        ?>
    </table>
    
    </main>
</body>