<?php
include 'profesorNavBar.php';
include 'dbconfig_og.php';

$msg = (isset($_GET["msg"])) ? $_GET["msg"]:null;

$idprofesor = $_SESSION["userid"];
$ramo; $load;
if ( isset($_SESSION["ramo"]) ){
    $ramo = $_SESSION["ramo"];
}
else{
    $query = "select siglaramo from imparticion where idprofesor = ".$idprofesor;
    $result = pg_query($dbconn,$query);
    if ($result){
        $ramo = pg_fetch_row($result)[0];
        if ($ramo) $_SESSION["ramo"] = $ramo;
    }
}
if ($ramo){
    $load = true;
    $ayquery = "select * from ayudantia where siglaramo = $1";
    $ayresult = pg_query_params($dbconn, $ayquery, array($ramo));
}
else{
    $load = false;
}
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <?php
        if ($msg == "err") echo "<div class=\"alert alert-danger\">Error al realizar su requierimiento</div>";
        elseif ($msg == "succ") echo "<div class=\"alert alert-success\">Se ha revocado al ayudante</div>";
    ?>
    <?php
        if (!$load) echo "<div class=\"alert alert-warning\">Usted no imparte ramos o hubo un error al obtener estos datos</div>";
        if ($load && !$ayresult) echo "<div class=\"alert alert-warning\">Error al obtener ayudantes de la base de datos</div>";
    ?>
    <div class="jumbotron text-center">
        <h1>Listado de Ayudantes</h1>
    </div>

    <table class="table table-bordered">
        <thead class="thead-dark">
            <th>Id de Ayudantia</th>
            <th>Rol Ayudante</th>
            <th>Sigla del Ramo</th>
            <th>Carga</th>
            <th>Editar Alumno</th>
            <th>Eliminar Ayudante</th>
        </thead>
    <?php
        echo "<tbody>";
        if ($ayresult){
            while ($ayudante = pg_fetch_row($ayresult) ){
                echo "<tr>";
                echo "<td>".$ayudante[0]."</td>";
                echo "<td>".$ayudante[1]."</td>";
                echo "<td>".$ayudante[2]."</td>";
                echo "<td>".$ayudante[3]."</td>";
                echo "<td align=\"center\"><a href=\"profesorUpdateAlumno.php?id=".$ayudante[1]."\">Editar Alumno</td>";
                echo "<td align=\"center\"><a href=\"updateController/deleteAyudante.php?id=".$ayudante[0]."\" onclick=\"return confirm('Estas seguro?');\">Revocar Ayudante</td>";
                echo "</tr>";
            }
        }
        echo "</tbody>";
        pg_close($dbconn);
    ?>
    </table>

    </main>
</body>