<?php
    include 'alumnoNavBar.php';
    include 'dbconfig_og.php';
    $msg = (isset($_GET["msg"])) ? $_GET["msg"] : null;
    
    $idayudantia = $_SESSION["idayudantia"];

    $query = 'select asignacion.idayudantia, mision.idmision, mision.idalumno, mision.descripcion, mision.recompensa, mision.estado from asignacion inner join mision on asignacion.idmision = mision.idmision where asignacion.idayudantia = $1';
    $result = pg_query_params($dbconn,$query,array($idayudantia));
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">

    <?php if (!$result){
        echo "<div class=\"alert alert-warning\">Error al comunicarse con la base de datos</div>";
        pg_close($dbconn);
    }
    else{   
    ?>
    <?php
        if ($msg == "err") echo "<div class=\"alert alert-danger\">No se ha podido cambiar el estado de la Mision</div>";
        elseif ($msg == "succ") echo "<div class=\"alert alert-success\">Estado cambiado satisfactoriamente</div>";
    ?>
    <div class="jumbotron text-center">
        <h1>Listado de Misiones</h1>
    </div>
    <table class="table table-bordered">
        <thead class="thead-dark">
            <th>Id Ayudantia</th>
            <th>Id Mision</th>
            <th>Id Alumno</th>
            <th>Descripcion</th>
            <th>Recompensa</th>
            <th>Estado</th>
            <th>Cambiar estado</th>
        </thead>
        <?php
            echo "<tbody>";
            while ($mision = pg_fetch_row($result)){
                echo "<tr>";
                echo "<td>".$mision[0]."</td>";
                echo "<td>".$mision[1]."</td>";
                echo "<td>".$mision[2]."</td>";
                echo "<td>".$mision[3]."</td>";
                echo "<td>".$mision[4]."</td>";
                if ($mision[5] == 1) echo "<td>COMPLETA</td>";
                else echo "<td>INCOMPLETA</td>";
                echo "<td align=\"center\"><a href=\"updateController/toggleMision.php?misionid=".$mision[1]."&state=".$mision[5]."\">Actualizar Estado</td>"; 
                echo "</tr>";
            }
            echo "</tbody>";
            pg_close($dbconn);
        }
        ?>
    </table>
    
    </main>
</body>
