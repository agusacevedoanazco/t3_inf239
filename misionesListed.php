<?php
include 'profesorNavBar.php';
include 'dbconfig_og.php';

$idprofesor = $_SESSION["userid"];

$query = 'select idmision, idalumno, descripcion, recompensa, estado from mision where idprofesor = $1;';

$result = pg_query_params($dbconn,$query,array($idprofesor));
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">

    <?php if (!$result){
        echo "<div class=\"alert alert-warning\">Error al comunicarse con la base de datos</div>";
        pg_close($dbconn);
    }
    else{
    ?>

    <div class="jumbotron text-center">
        <h1>Listado de Misiones</h1>
    </div>
    <table class="table table-bordered">
        <thead class="thead-dark">
            <th>Id Mision</th>
            <th>Rol Alumno</th>
            <th>Descripcion</th>
            <th>Recompensa</th>
            <th>Estado</th>
            <th>Editar Mision</th>
        </thead>
        <?php
            echo "<tbody>";
            while ($mision = pg_fetch_row($result)){
                echo "<tr>";
                echo "<td>".$mision[0]."</td>";
                echo "<td>".$mision[1]."</td>";
                echo "<td>".$mision[2]."</td>";
                echo "<td>".$mision[3]."</td>";
                if ($mision[4] == 1) echo "<td>COMPLETA</td>";
                else echo "<td>INCOMPLETA</td>";
                echo "<td align=\"center\"><a href=\"modifyMision.php?misionid=".$mision[0]."\">Modificar Mision</td>"; 
                echo "</tr>";
            }
            echo "</tbody>";
            pg_close($dbconn);
        }
        ?>
    </table>
    
    </main>
</body>