<?php
include "../dbconfig_og.php";

session_start();
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $userid = filter_var($_POST["alumnoId"], FILTER_SANITIZE_STRING);
    $passw = filter_var($_POST["alumnoPw"], FILTER_SANITIZE_STRING);
    $query = 'select rolalumno, passw from alumno where rolalumno = $1';
    $result = pg_query_params($dbconn, $query, array($userid));
    
    if(!$result) header('Location: ../index.php?msg=baddb');

    $user = pg_fetch_row($result);

    pg_close($dbconn);

    if ($user == NULL){
        session_destroy();
        header('Location: ../index.php?msg=baduser');
    }
    else{
        if ($userid == $user[0] && password_verify($passw,$user[1]) ){
            $_SESSION["type"] = "alumno";
            $_SESSION["userid"] = $userid;
            header('Location: ../homeAlumno.php');
        }
        else header('Location: ../index.php?msg=baduser');
    }
}
else{
    pg_close($dbconn);
    header('Location: ../index.php');
}

?>