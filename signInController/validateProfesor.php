<?php
    session_start();
    if( !isset($_SESSION["userid"]) || !isset($_SESSION["type"])){
        header('Location: ../index.php');
    }
    else{
        if ($_SESSION["type"] != "profesor"){
            header('Location: ../index.php');
        }
    }
?>