<?php
    include "../dbconfig_og.php";

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    if (!isset($_POST["alumnoId"]) || !isset($_POST["alumnoPw"]) ){
        pg_close($dbconn);
        header('Location: ../index.php?msg=regErr');
    }
    else{
        $id = filter_var($_POST["alumnoId"], FILTER_SANITIZE_STRING);

        $fndquery = 'select rolalumno from alumno where rolalumno = $1';
        $fndresult = pg_query_params($dbconn, $fndquery, array($id));
        
        if (pg_fetch_row($fndresult)[0] != FALSE) header('Location: ../index.php?msg=regerr');
        else{
            $passw = password_hash( filter_var($_POST["alumnoPw"], FILTER_SANITIZE_STRING), PASSWORD_BCRYPT, array('cost'=>12));
            $name = (!isset($_POST["name"])) ? null : filter_var($_POST["name"], FILTER_SANITIZE_STRING);
            $lname = (!isset($_POST["lname"])) ? null : filter_var($_POST["lname"], FILTER_SANITIZE_STRING);
            $eyear = (!isset($_POST["eyear"])) ? null : filter_var($_POST["eyear"], FILTER_SANITIZE_NUMBER_INT);

            $rquery = 'insert into alumno (rolalumno, nombre, apellido, anoingreso, passw) values ($1,$2,$3,$4,$5)';
            $result = pg_query_params($dbconn, $rquery, array($id,$name,$lname,$eyear,$passw));

            if (!$fndresult) header('Location: ../index.php?msg=baddb');
            else header('Location: ../index.php?msg=regsucc');
        }
    }
}
    
else{
    pg_close($dbconn);
    header('Location: ../index.php');
}    
?>