<?php
include "../dbconfig_og.php";

session_start();
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $userid = filter_var($_POST["profesorId"], FILTER_SANITIZE_NUMBER_INT);
    $passw = filter_var($_POST["profesorPw"], FILTER_SANITIZE_STRING);
    $query = 'select idprofesor, passw from profesor where idprofesor = $1';
    $result = pg_query_params($dbconn, $query, array($userid));
    
    if(!$result) header('Location: ../index.php?msg=baddb');

    $user = pg_fetch_row($result);

    pg_close($dbconn);

    if ($user == NULL){
        session_destroy();
        header('Location: ../index.php?msg=baduser');
    }
    else{
        if ($userid == $user[0] && password_verify($passw,$user[1]) ){
            $_SESSION["type"] = "profesor";
            $_SESSION["userid"] = $userid;
            header('Location: ../homeProfesor.php');
        }
        else header('Location: ../index.php?msg=baduser');
    }
}
header('Location: ../index.php');

?>