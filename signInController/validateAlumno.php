<?php
    include 'dbconfig_og.php';

    session_start();
    if( !isset($_SESSION["userid"]) || !isset($_SESSION["type"])){
        header('Location: ../index.php');
    }
    else{
        if ($_SESSION["type"] != "alumno"){
            header('Location: ../index.php');
        }
        else{
            //cada vez que se inicia sesión se chequea si es ayudante o no
            $query = 'select idayudantia from ayudantia where rolayudante = $1';
            $result = pg_query_params($dbconn,$query,array($_SESSION["userid"]));
            if (!$result){
                header("Location ../index.php?msg=baddb");
            }
            else{
                $ayudantia = pg_fetch_row($result);
                if($ayudantia){
                    $_SESSION["role"] = "ayudante";
                    $_SESSION["idayudantia"] = $ayudantia[0];
                }
                else{
                    $_SESSION["role"] = "alumno";
                }
            }
        }
    }
    pg_close($dbconn);
?>