<?php
/**
 * Eliminar ayudante ingresado
 */
include '../dbconfig_og.php';

session_start();
if ( ( !isset($_SESSION["userid"]) ) || ( !isset($_SESSION["type"]) || $_SESSION["type"] != "profesor" ) ){
    //Si el usuario no está seteado OR es otro tipo que no sea alumno, entonces, la sesion no es valida y no puede actualizar al alumno
    pg_close($dbconn);
    session_destroy();
    header('Location: ../index.php');
}
else{
    if (isset($_GET["id"])){
        $ayid = $_GET["id"];
        $query1 = "delete from asignacion where idayudantia = $1";
        $query2 = "delete from ayudantia where idayudantia = $1";
        pg_query_params($dbconn,$query1,array($ayid)); //si tiene asignacion elimina al ayudante de la asignacion sino devuelve error. Por tanto el resultado no interesa
        $result = pg_query_params($dbconn,$query2,array($ayid));
        pg_close($dbconn);
        if (!$result){
            header('Location: ../ayudantesListed.php?msg=err');
        }
        else{
            header('Location: ../ayudantesListed.php?msg=succ');
        }
    }
    else{
        header('Location: ../index.php');
    }
}
?>