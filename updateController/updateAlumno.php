<?php
/**
 * Actualiza informacion del alumno logueado
 */
include '../dbconfig_og.php';

session_start();
if ( ( !isset($_SESSION["userid"]) ) || ( !isset($_SESSION["type"]) || $_SESSION["type"] != "alumno" ) ){
    //Si el usuario no está seteado OR es otro tipo que no sea alumno, entonces, la sesion no es valida y no puede actualizar al alumno
    pg_close($dbconn);
    session_destroy();
    header('Location: ../index.php');
}
else{
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        $userid = $_SESSION["userid"];
        $passw = password_hash( filter_var($_POST["passw"], FILTER_SANITIZE_STRING), PASSWORD_BCRYPT, array('cost'=>12));
        $name = (!isset($_POST["nombre"])) ? null : filter_var($_POST["nombre"], FILTER_SANITIZE_STRING);
        $lname = (!isset($_POST["apellido"])) ? null : filter_var($_POST["apellido"], FILTER_SANITIZE_STRING);
        $eyear = (!isset($_POST["anoIngreso"])) ? null : filter_var($_POST["anoIngreso"], FILTER_SANITIZE_NUMBER_INT);

        $query = "update alumno set nombre = $1, apellido = $2, anoingreso = $3, passw = $4 where rolalumno = $5";

        $result = pg_query_params($dbconn, $query, array($name,$lname,$eyear,$passw,$userid));

        pg_close($dbconn);

        if (!$result){
            echo "<p>NACK</p>";
            header('Location:  ../homeAlumno.php?msg=baddb');
        }
        else{
            echo "<p>ACK</p>";
            header('Location: ../homeAlumno.php?msg=updok');
        }
    }
    else header('Location: ../index.php');
}
?>