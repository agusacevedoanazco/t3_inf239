<?php
/**
 * Actualiza informacion del profesor logueado
 */
include '../dbconfig_og.php';

session_start();
if ( ( !isset($_SESSION["userid"]) ) || ( !isset($_SESSION["type"]) || $_SESSION["type"] != "profesor" ) ){
    //Si el usuario no está seteado OR es otro tipo que no sea profesor, entonces, la sesion no es valida y no puede actualizar datos del profesor
    pg_close($dbconn);
    session_destroy();
    header('Location: ../index.php');
}
else{
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        $userid = $_SESSION["userid"];
        $passw = password_hash( filter_var($_POST["passw"], FILTER_SANITIZE_STRING), PASSWORD_BCRYPT, array('cost'=>12));
        $name = (!isset($_POST["nombre"])) ? null : filter_var($_POST["nombre"], FILTER_SANITIZE_STRING);
        $lname = (!isset($_POST["apellido"])) ? null : filter_var($_POST["apellido"], FILTER_SANITIZE_STRING);
        $esp = (!isset($_POST["especialidad"])) ? null : filter_var($_POST["especialidad"], FILTER_SANITIZE_STRING);

        $query = "update profesor set nombre = $1, apellido = $2, especialidad = $3, passw = $4 where idprofesor = $5";

        $result = pg_query_params($dbconn, $query, array($name,$lname,$esp,$passw,$userid));

        pg_close($dbconn);

        if (!$result){
            echo "<p>NACK</p>";
            header('Location:  ../homeProfesor.php?msg=baddb');
        }
        else{
            echo "<p>ACK</p>";
            header('Location: ../homeProfesor.php?msg=updok');
        }
    }
    else header('Location: ../index.php');
}
?>