<?php
/**
 * Actualiza informacion dela mision
 */
include '../dbconfig_og.php';

session_start();
if ( ( !isset($_SESSION["userid"]) ) || ( !isset($_SESSION["type"]) || $_SESSION["type"] != "profesor" ) ){
    //Si el usuario no está seteado OR es otro tipo que no sea profesor, entonces, la sesion no es valida y no puede actualizar la mision
    pg_close($dbconn);
    session_destroy();
    header('Location: ../index.php');
}
else {
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET["mid"])){
        $idm = $_GET["mid"];
        $desc = filter_var($_POST["descripcion"], FILTER_SANITIZE_STRING);
        $rew = filter_var($_POST["recompensa"], FILTER_SANITIZE_STRING);

        $query = 'update mision set recompensa = $1, descripcion = $2 where idmision = $3';

        $result = pg_query_params($dbconn,$query,array($rew,$desc,$idm));

        pg_close($dbconn);
        if (!$result) header('Location: ../modifyMision.php?msg=err');
        else header('Location: ../modifyMision.php?msg=succ');
    }
    else header('Location: ../index.php');
}
?>
