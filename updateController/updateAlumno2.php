<?php
/**
 * Actualiza informacion del alumno desde profesor
 */
include '../dbconfig_og.php';

session_start();
if ( ( !isset($_SESSION["userid"]) ) || ( !isset($_SESSION["type"]) || $_SESSION["type"] != "profesor" ) ){
    //Si el usuario no está seteado OR es otro tipo que no sea profesor, entonces, la sesion no es valida y no puede actualizar al alumno
    pg_close($dbconn);
    session_destroy();
    header('Location: ../index.php');
}
else {
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET["id"])){
        $alumnoid = filter_var($_GET["id"], FILTER_SANITIZE_STRING);
        $name = (!isset($_POST["nombre"])) ? null : filter_var($_POST["nombre"], FILTER_SANITIZE_STRING);
        $lname = (!isset($_POST["apellido"])) ? null : filter_var($_POST["apellido"], FILTER_SANITIZE_STRING);
        $eyear = (!isset($_POST["aingreso"])) ? null : filter_var($_POST["aingreso"], FILTER_SANITIZE_NUMBER_INT);

        $query = "update alumno set nombre = $1, apellido = $2, anoingreso = $3 where rolalumno = $4;";

        $result = pg_query_params($dbconn, $query, array($name,$lname,$eyear,$alumnoid));

        pg_close($dbconn);

        if (!$result){
            echo "<p>NACK</p>";
            header('Location:  ../profesorUpdateAlumno.php?msg=baddb');
        }
        else{
            echo "<p>ACK</p>";
            header('Location: ../profesorUpdateAlumno.php?msg=okupd8&id='.$alumnoid);
        }
    }
    else header('Location: ../index.php');
}