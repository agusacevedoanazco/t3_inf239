<?php
    include "../dbconfig_og.php";

    session_start();
    if ( ( !isset($_SESSION["userid"]) ) || ( !isset($_SESSION["type"]) || $_SESSION["type"] != "alumno" ) || ( !isset($_SESSION["role"]) || $_SESSION["role"] != "ayudante" ) ){
        //Si el usuario no está seteado OR el tipo de usuario no es alumno OR el rol del alumno NO es ayudante, entonces no tiene acceso valido
        pg_close($dbconn);
        session_destroy();
        header('Location: ../index.php');
    }
    else{
        if(!isset($_GET["misionid"]) || !isset($_GET["state"])){
            header('Location: ../misMisiones.php?msg=err');
        }
        else{
            $idm = $_GET["misionid"];
            $state = ($_GET["state"]) ? 0 : 1;
            $query = 'update mision set estado = $1 where idmision = $2';
            $result = pg_query_params($dbconn,$query,array($state,$idm));

            pg_close($dbconn);
            if (!$result) header('Location: ../misMisiones.php?msg=err');
            else header('Location: ../misMisiones.php?msg=succ');
        }
    }