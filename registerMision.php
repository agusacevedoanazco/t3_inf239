<?php
include 'profesorNavBar.php'; 
$msg = (isset($_GET["msg"])) ? $_GET["msg"]: null;
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <?php
        if ($msg == "nack") echo "<div class=\"alert alert-warning\">Error al comunicarse con la base de datos</div>";
        elseif ($msg == "stnfnd") echo "<div class=\"alert alert-warning\">El estudiante ingresado no pudo ser identificado</div>";
        elseif ($msg == "ack") echo "<div class=\"alert alert-success\">Mision añadida exitosamente</div>";
    ?>
    <div class="jumbotron text-center">
        <h1>Añadir una nueva Misión</h1>
    </div>
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="registerController/newMision.php" method="POST">
                    <div class="form-group">
                        <label for="alumnoid">Rol Alumno</label>
                        <input type="text" class="form-control" name="alumnoid" id="alumnoid" required>
                    </div>

                    <div class="form-group">
                        <label for="descripcion">Descripcion</label>
                        <input type="text" class="form-control" name="descripcion" id="descripcion" required>
                    </div>

                    <div class="form-group">
                        <label for="recompensa">Recompensa</label>
                        <input type="text" class="form-control" name="recompensa" id="recompensa" required>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Añadir</button>
                </form>
            </div>
        </div>
    </div>
</main>

</body>