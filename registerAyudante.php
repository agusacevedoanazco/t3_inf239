<?php
include 'profesorNavBar.php';
include 'dbconfig_og.php';

$msg = (isset($_GET["msg"])) ? $_GET["msg"]:null;

$idprofesor = $_SESSION["userid"];
$ramo; $load;
if ( isset($_SESSION["ramo"]) ){
    $ramo = $_SESSION["ramo"];
}
else{
    $query = "select siglaramo from imparticion where idprofesor = ".$idprofesor;
    $result = pg_query($dbconn,$query);
    if ($result){
        $ramo = pg_fetch_row($result)[0];
        if ($ramo) $_SESSION["ramo"] = $ramo;
    }
}
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <?php
        if (!$ramo) echo "<div class=\"alert alert-warning\">Usted no imparte ramos o hubo un error al obtener estos datos</div>";
        else{
    ?>
    <?php
        if ($msg == "err") echo "<div class=\"alert alert-danger\">Hubo un error al intentar agregar al ayudante</div>";
        elseif ($msg == "succ") echo "<div class=\"alert alert-success\">Ayudante agregado correctamente</div>";
        elseif ($msg == "dplct") echo "<div class=\"alert alert-warning\">Alumno ingresado ya es ayudante de la asignatura</div>"
    ?>
    <div class="jumbotron text-center">
        <h1>Registrar nuevo Ayudante</h1>
    </div>
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="registerController/newAyudante.php" method="POST">
                    <div class="form-group">
                        <label for="ayudantiaid">Id de Ayudantia</label>
                        <input type="text" class="form-control" name="idayudantia" id="idayudantia" required>
                    </div>

                    <div class="form-group">
                        <label for="rolalumno">Rol Alumno</label>
                        <input type="text" class="form-control" name="rolalumno" id="rolalumno" required>
                    </div>

                    <div class="form-group">
                        <label for="carga">Carga Académica</label>
                        <input type="number" class="form-control" name="carga" id="carga">
                    </div>

                    <button type="submit" class="btn btn-primary">Registrar</button>
                </form>
            </div>
        </div>
    </div>
    <?php
        }
    ?>
</main>
</body>