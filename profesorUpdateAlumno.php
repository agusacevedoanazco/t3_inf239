<?php
include 'profesorNavBar.php';
$msg = (isset($_GET["msg"])) ? $_GET["msg"] : null;
$alumnoid = (isset($_GET["id"])) ? $_GET["id"] : null;
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <?php
        if ($msg == "baddb") echo "<div class=\"alert alert-warning\">Error al conectarse a la base de datos</div>";
        elseif ($msg == "okupd8") echo "<div class=\"alert alert-success\">Alumno: ".$alumnoid.", actualizado correctamente.</div>";
    ?>
    <div class="jumbotron text-center">
        <h1>Modificar Alumno</h1>
    </div>
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="updateController/updateAlumno2.php?id=<?php echo $alumnoid?>" method="POST">
                    <div class="form-group">
                        <label for="rolalumno">Rol Alumno</label>
                        <input type="text" class="form-control" name="rolalumno" id="rolalumno" placeholder="<?php echo $alumnoid;?>" required disabled>
                    </div>    

                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" required>
                    </div>

                    <div class="form-group">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" name="apellido" id="apellido" required>
                    </div>

                    <div class="form-group">
                        <label for="aingreso">Año de Ingreso</label>
                        <input type="number" class="form-control" name="aingreso" id="aingreso" required>
                    </div>

                    <button type="submit" class="btn btn-primary">Actualizar</button>
                </form>
            </div>
        </div>
    </div>
</main>

</body>