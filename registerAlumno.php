<?php include 'header.php';?>

<link href="css/signin.css" rel="stylesheet">
<body class="text-center">
    <div class="mock-body">
        <form class="form-signin" method="post" action="signInController/signUpAlumno.php">
            <h1 class="h3 mb-3 font-weight-normal">Registrar Nuevo Alumno</h1>
            <input type="text" name="alumnoId" id="alumnoId" class="form-control" placeholder="Rol Alumno" required autofocus>
            <input type="text" name="name" id="name" class="form-control" placeholder="Nombre">
            <input type="text" name="lname" id="lname" class="form-control" placeholder="Apellido">
            <input type="number" name="eyear" id="eyear" class="form-control" placeholder="Año de Ingreso">
            <input type="password" name="alumnoPw" id="alumnoPw" class="form-control" placeholder="Contraseña" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Registrar</button>
        </form>
    </div>
</body>