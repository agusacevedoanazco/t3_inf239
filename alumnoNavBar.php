<?php
    include 'header.php';
    include 'signInController/validateAlumno.php';
    $role = $_SESSION["role"];
?>

<link href="css/navbars.css" rel="stylesheet">
<body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="index.php">TUMA II: Electric Boogaloo</a>
        <!---<input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">-->
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="signInController/signOut.php">Sign out</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
                <div class="sidebar-sticky pt-3">
                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Mi Perfil</span>
                    </h6>
                    <ul class="nav flex-column">
                        <li class="nav-item"><a class="nav-link" href="homeAlumno.php">Inicio</a></li>
                    </ul>
                    
                    <?php if ($role == "ayudante") {?>
                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Administrar Misiones</span>
                    </h6>
                    <ul class="nav flex-column">
                        <li class="nav-item"><a class="nav-link" href="aceptarMision.php">Aceptar Misiones</a></li>
                        <li class="nav-item"><a class="nav-link" href="misMisiones.php">Listado de Misiones</a></li>
                    </ul>
                    <?php } //ENDIF?>
                </div>
            </nav>
        </div>
    </div>
