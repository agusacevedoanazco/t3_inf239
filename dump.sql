PGDMP     ,                    x            tarea3inf239     12.3 (Ubuntu 12.3-1.pgdg18.04+1)     12.3 (Ubuntu 12.3-1.pgdg18.04+1) )    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16515    tarea3inf239    DATABASE     ~   CREATE DATABASE tarea3inf239 WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE tarea3inf239;
                postgres    false            �            1259    16594    alumno    TABLE     �   CREATE TABLE public.alumno (
    rolalumno character varying(15) NOT NULL,
    nombre character varying(45),
    apellido character varying(45),
    anoingreso integer,
    passw character varying(400)
);
    DROP TABLE public.alumno;
       public         heap    postgres    false            �            1259    16637 
   asignacion    TABLE     r   CREATE TABLE public.asignacion (
    idayudantia character varying(15) NOT NULL,
    idmision integer NOT NULL
);
    DROP TABLE public.asignacion;
       public         heap    postgres    false            �            1259    16622 	   ayudantia    TABLE     �   CREATE TABLE public.ayudantia (
    idayudantia character varying(15) NOT NULL,
    rolayudante character varying(15),
    siglaramo character varying(7),
    carga integer
);
    DROP TABLE public.ayudantia;
       public         heap    postgres    false            �            1259    16652    imparticion    TABLE     r   CREATE TABLE public.imparticion (
    idprofesor integer NOT NULL,
    siglaramo character varying(7) NOT NULL
);
    DROP TABLE public.imparticion;
       public         heap    postgres    false            �            1259    16604    mision    TABLE     .  CREATE TABLE public.mision (
    idmision integer NOT NULL,
    idprofesor integer,
    idalumno character varying(15),
    fechaingreso timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    descripcion character varying(90),
    recompensa character varying(60),
    estado smallint DEFAULT 0
);
    DROP TABLE public.mision;
       public         heap    postgres    false            �            1259    16602    mision_idmision_seq    SEQUENCE     �   CREATE SEQUENCE public.mision_idmision_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.mision_idmision_seq;
       public          postgres    false    207            �           0    0    mision_idmision_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.mision_idmision_seq OWNED BY public.mision.idmision;
          public          postgres    false    206            �            1259    16580    profesor    TABLE     �   CREATE TABLE public.profesor (
    idprofesor integer NOT NULL,
    nombre character varying(45),
    apellido character varying(45),
    especialidad character varying(45),
    passw character varying(400)
);
    DROP TABLE public.profesor;
       public         heap    postgres    false            �            1259    16578    profesor_idprofesor_seq    SEQUENCE     �   CREATE SEQUENCE public.profesor_idprofesor_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.profesor_idprofesor_seq;
       public          postgres    false    203            �           0    0    profesor_idprofesor_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.profesor_idprofesor_seq OWNED BY public.profesor.idprofesor;
          public          postgres    false    202            �            1259    16589    ramo    TABLE     �   CREATE TABLE public.ramo (
    siglaramo character varying(7) NOT NULL,
    nombre character varying(45),
    creditossct integer,
    semestre character varying(6)
);
    DROP TABLE public.ramo;
       public         heap    postgres    false                       2604    16607    mision idmision    DEFAULT     r   ALTER TABLE ONLY public.mision ALTER COLUMN idmision SET DEFAULT nextval('public.mision_idmision_seq'::regclass);
 >   ALTER TABLE public.mision ALTER COLUMN idmision DROP DEFAULT;
       public          postgres    false    206    207    207                       2604    16583    profesor idprofesor    DEFAULT     z   ALTER TABLE ONLY public.profesor ALTER COLUMN idprofesor SET DEFAULT nextval('public.profesor_idprofesor_seq'::regclass);
 B   ALTER TABLE public.profesor ALTER COLUMN idprofesor DROP DEFAULT;
       public          postgres    false    202    203    203            �          0    16594    alumno 
   TABLE DATA           P   COPY public.alumno (rolalumno, nombre, apellido, anoingreso, passw) FROM stdin;
    public          postgres    false    205   /1       �          0    16637 
   asignacion 
   TABLE DATA           ;   COPY public.asignacion (idayudantia, idmision) FROM stdin;
    public          postgres    false    209   �1       �          0    16622 	   ayudantia 
   TABLE DATA           O   COPY public.ayudantia (idayudantia, rolayudante, siglaramo, carga) FROM stdin;
    public          postgres    false    208   2       �          0    16652    imparticion 
   TABLE DATA           <   COPY public.imparticion (idprofesor, siglaramo) FROM stdin;
    public          postgres    false    210   V2       �          0    16604    mision 
   TABLE DATA           o   COPY public.mision (idmision, idprofesor, idalumno, fechaingreso, descripcion, recompensa, estado) FROM stdin;
    public          postgres    false    207   |2       �          0    16580    profesor 
   TABLE DATA           U   COPY public.profesor (idprofesor, nombre, apellido, especialidad, passw) FROM stdin;
    public          postgres    false    203   �2       �          0    16589    ramo 
   TABLE DATA           H   COPY public.ramo (siglaramo, nombre, creditossct, semestre) FROM stdin;
    public          postgres    false    204   `4       �           0    0    mision_idmision_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.mision_idmision_seq', 2, true);
          public          postgres    false    206            �           0    0    profesor_idprofesor_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.profesor_idprofesor_seq', 7, true);
          public          postgres    false    202                       2606    16601    alumno alumno_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.alumno
    ADD CONSTRAINT alumno_pkey PRIMARY KEY (rolalumno);
 <   ALTER TABLE ONLY public.alumno DROP CONSTRAINT alumno_pkey;
       public            postgres    false    205                       2606    16641    asignacion asignacion_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.asignacion
    ADD CONSTRAINT asignacion_pkey PRIMARY KEY (idayudantia, idmision);
 D   ALTER TABLE ONLY public.asignacion DROP CONSTRAINT asignacion_pkey;
       public            postgres    false    209    209                       2606    16626    ayudantia ayudantia_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.ayudantia
    ADD CONSTRAINT ayudantia_pkey PRIMARY KEY (idayudantia);
 B   ALTER TABLE ONLY public.ayudantia DROP CONSTRAINT ayudantia_pkey;
       public            postgres    false    208                        2606    16656    imparticion imparticion_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.imparticion
    ADD CONSTRAINT imparticion_pkey PRIMARY KEY (idprofesor, siglaramo);
 F   ALTER TABLE ONLY public.imparticion DROP CONSTRAINT imparticion_pkey;
       public            postgres    false    210    210                       2606    16611    mision mision_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.mision
    ADD CONSTRAINT mision_pkey PRIMARY KEY (idmision);
 <   ALTER TABLE ONLY public.mision DROP CONSTRAINT mision_pkey;
       public            postgres    false    207                       2606    16588    profesor profesor_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.profesor
    ADD CONSTRAINT profesor_pkey PRIMARY KEY (idprofesor);
 @   ALTER TABLE ONLY public.profesor DROP CONSTRAINT profesor_pkey;
       public            postgres    false    203                       2606    16593    ramo ramo_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.ramo
    ADD CONSTRAINT ramo_pkey PRIMARY KEY (siglaramo);
 8   ALTER TABLE ONLY public.ramo DROP CONSTRAINT ramo_pkey;
       public            postgres    false    204            %           2606    16642 &   asignacion asignacion_idayudantia_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.asignacion
    ADD CONSTRAINT asignacion_idayudantia_fkey FOREIGN KEY (idayudantia) REFERENCES public.ayudantia(idayudantia);
 P   ALTER TABLE ONLY public.asignacion DROP CONSTRAINT asignacion_idayudantia_fkey;
       public          postgres    false    209    2844    208            &           2606    16647 #   asignacion asignacion_idmision_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.asignacion
    ADD CONSTRAINT asignacion_idmision_fkey FOREIGN KEY (idmision) REFERENCES public.mision(idmision);
 M   ALTER TABLE ONLY public.asignacion DROP CONSTRAINT asignacion_idmision_fkey;
       public          postgres    false    207    209    2842            #           2606    16627 $   ayudantia ayudantia_rolayudante_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ayudantia
    ADD CONSTRAINT ayudantia_rolayudante_fkey FOREIGN KEY (rolayudante) REFERENCES public.alumno(rolalumno);
 N   ALTER TABLE ONLY public.ayudantia DROP CONSTRAINT ayudantia_rolayudante_fkey;
       public          postgres    false    205    208    2840            $           2606    16632 "   ayudantia ayudantia_siglaramo_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ayudantia
    ADD CONSTRAINT ayudantia_siglaramo_fkey FOREIGN KEY (siglaramo) REFERENCES public.ramo(siglaramo);
 L   ALTER TABLE ONLY public.ayudantia DROP CONSTRAINT ayudantia_siglaramo_fkey;
       public          postgres    false    208    2838    204            '           2606    16657 '   imparticion imparticion_idprofesor_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.imparticion
    ADD CONSTRAINT imparticion_idprofesor_fkey FOREIGN KEY (idprofesor) REFERENCES public.profesor(idprofesor);
 Q   ALTER TABLE ONLY public.imparticion DROP CONSTRAINT imparticion_idprofesor_fkey;
       public          postgres    false    203    210    2836            (           2606    16662 &   imparticion imparticion_siglaramo_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.imparticion
    ADD CONSTRAINT imparticion_siglaramo_fkey FOREIGN KEY (siglaramo) REFERENCES public.ramo(siglaramo);
 P   ALTER TABLE ONLY public.imparticion DROP CONSTRAINT imparticion_siglaramo_fkey;
       public          postgres    false    2838    204    210            "           2606    16617    mision mision_idalumno_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mision
    ADD CONSTRAINT mision_idalumno_fkey FOREIGN KEY (idalumno) REFERENCES public.alumno(rolalumno);
 E   ALTER TABLE ONLY public.mision DROP CONSTRAINT mision_idalumno_fkey;
       public          postgres    false    205    207    2840            !           2606    16612    mision mision_idprofesor_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mision
    ADD CONSTRAINT mision_idprofesor_fkey FOREIGN KEY (idprofesor) REFERENCES public.profesor(idprofesor);
 G   ALTER TABLE ONLY public.mision DROP CONSTRAINT mision_idprofesor_fkey;
       public          postgres    false    207    2836    203            �   �   x�=���0 ��;hŗ0��R
�/;����_/��n���#� V֨��$Q^<�aR�D�Nc�y���컈�΢m:_UO�����/��0�b/�Q9�����*3�)ec!F`�@���D��᯼4\f�<s��]Vm���ĉ��'ϸ{5�,R��y���s���o�~���G�R�[�;O      �      x�K��76��5�4����� "E*      �   (   x�K��76��5�42046240��5�Lr�r��qqq �H<      �      x�3�L��76������ ��      �   g   x�M���0��Y�E�u�Y�`�x B�)H�9��jT�]�LT��~������^{y�___����IY��q��6�lW�n_;R�I�_w^|Z)�c�A      �   ]  x�=��v�0�ux�( F�P���B=�%%C��/��nfo�(���  &�}���bN�R�ч�	;���FgTM�'�T6S���x���]��snЗ� G��<G�P>�(�Ѧi|?����7�V�\6+۽o�+qmEP�/�mW��I:��>h4��8�C���<xe�5G�����w���� ��/��T}k?��3�z��U�I`Ь'��� 6�&a��������L�!��d���O����$�"�����Y�)�%� Iڰ��xAuMhV��ZG��V�ua����v�N2����a�Vj�X��]g�77�coiD��%I�X_��      �   4   x�K��76��(�O/J�ML���SHIU�,.I�M,�4�4204����� G��     