<?php
    include "../dbconfig_og.php";

session_start();
if ( ( !isset($_SESSION["userid"]) ) || ( !isset($_SESSION["type"]) || $_SESSION["type"] != "profesor" ) ){
    //Si el usuario no está seteado OR es otro tipo que no sea profesor, entonces, la sesion no es valida y no puede crear datos del profesor
    pg_close($dbconn);
    session_destroy();
    header('Location: ../index.php');
}
else{
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        $idayudantia = filter_var($_POST["idayudantia"],FILTER_SANITIZE_STRING);
        $rolayudante = filter_var($_POST["rolalumno"],FILTER_SANITIZE_STRING);
        $siglaramo = $_SESSION["ramo"];
        $carga = (isset($_POST["carga"])) ? filter_var($_POST["carga"],FILTER_SANITIZE_NUMBER_INT) : null;
        if (!isset($idayudantia) || !isset($rolayudante) || !isset($siglaramo)){
            header('Location: ../registerAyudante.php?msg=err');
        }
        
        $fndquery = 'select idayudantia from ayudantia where rolayudante = $1';
        $fndresult = pg_query_params($dbconn,$fndquery,array($rolayudante));
        if(!$fndresult){
            header('Location: ../registerAyudante.php?msg=err');
        }
        if (pg_fetch_all($fndresult)[0]){
            pg_close($dbconn);
            header('Location: ../registerAyudante.php?msg=dplct');
        }
        else{
            $query = 'insert into ayudantia (idayudantia,rolayudante,siglaramo,carga) values ($1,$2,$3,$4)';
            $result = pg_query_params($dbconn,$query,array($idayudantia,$rolayudante,$siglaramo,$carga));
    
            pg_close($dbconn);
            if (!$result) header('Location: ../registerAyudante.php?msg=err');
            else header('Location: ../registerAyudante.php?msg=succ');
        }
    }
    else header('Location: ../index.php');
}