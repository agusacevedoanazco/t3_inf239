<?php
    include "../dbconfig_og.php";

session_start();
if ( ( !isset($_SESSION["userid"]) ) || ( !isset($_SESSION["type"]) || $_SESSION["type"] != "profesor" ) ){
    //Si el usuario no está seteado OR es otro tipo que no sea profesor, entonces, la sesion no es valida y no puede crear datos del profesor
    pg_close($dbconn);
    session_destroy();
    header('Location: ../index.php');
}
else{
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        $alid = filter_var($_POST["alumnoid"], FILTER_SANITIZE_STRING);
        $fndquery = 'select rolalumno from alumno where rolalumno = $1';
        $fndresult = pg_query_params($dbconn, $fndquery, array($alid));
        
        if (pg_fetch_row($fndresult)[0] == FALSE){
            pg_close($dbconn);
            header('Location: ../registerMision.php?msg=stnfnd');
        }
        else{
            $idpf = $_SESSION["userid"];

            $desc = filter_var($_POST["descripcion"], FILTER_SANITIZE_STRING);
            $rew = filter_var($_POST["recompensa"], FILTER_SANITIZE_STRING);
            
            $query = "insert into mision (idprofesor, idalumno, descripcion, recompensa) values ($1,$2,$3,$4);";

            $result = pg_query_params($dbconn,$query,array($idpf,$alid,$desc,$rew));

            if (!$result){
                pg_close($dbconn);
                header('Location: ../registerMision.php?msg=nack');
            }
            else{
                pg_close($dbconn);
                header('Location: ../registerMision.php?msg=ack');
            }
        }
    }
    else header('Location: ../index.php');
}
?>