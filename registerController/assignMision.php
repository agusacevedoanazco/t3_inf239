<?php
    include "../dbconfig_og.php";

    session_start();
    if ( ( !isset($_SESSION["userid"]) ) || ( !isset($_SESSION["type"]) || $_SESSION["type"] != "alumno" ) || ( !isset($_SESSION["role"]) || $_SESSION["role"] != "ayudante" ) ){
        //Si el usuario no está seteado OR el tipo de usuario no es alumno OR el rol del alumno NO es ayudante, entonces no tiene acceso valido
        pg_close($dbconn);
        session_destroy();
        header('Location: ../index.php');
    }
    else{
        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            if (!isset($_SESSION["idayudantia"])){
                pg_close($dbconn);
                header('Location: ../aceptarMision.php?msg=nullay');
            }
            else{
                $userid = $_SESSION["userid"];
                $idmision = filter_var($_POST["idmision"],FILTER_SANITIZE_NUMBER_INT);
                $idayudantia = $_SESSION["idayudantia"];

                $fndquery = "select idalumno from mision where idmision = $1";
                $result = pg_query_params($dbconn,$fndquery,array($idmision));
                
                if (!$result){
                    pg_close($dbconn);
                    header('Location: ../aceptarMision.php?msg=baddb');
                }

                if (pg_fetch_row($result)[0] == $userid){
                    pg_close($dbconn);
                    header('Location: ../aceptarMision.php?msg=denied');
                }
                else{
                    $query = "insert into asignacion (idayudantia, idmision) values ($1,$2)";
                    $newresult = pg_query_params($dbconn,$query,array($idayudantia,$idmision));
                    if (!$newresult){
                        pg_close($dbconn);
                        header('Location: ../aceptarMision.php?msg=baddb');
                    }
                    else{
                        pg_close($dbconn);
                        header('Location: ../aceptarMision.php?msg=succ');
                    }
                }
            }
        }
        else header('Location: ../index.php');
    }
?>