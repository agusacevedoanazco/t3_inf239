<?php
    include "../dbconfig_og.php";

session_start();
if ( ( !isset($_SESSION["userid"]) ) || ( !isset($_SESSION["type"]) || $_SESSION["type"] != "profesor" ) ){
    //Si el usuario no está seteado OR es otro tipo que no sea profesor, entonces, la sesion no es valida y no puede crear datos del profesor
    pg_close($dbconn);
    session_destroy();
    header('Location: ../index.php');
}
else{
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        $passw = password_hash( filter_var($_POST["passw"], FILTER_SANITIZE_STRING), PASSWORD_BCRYPT, array('cost'=>12));
        $name = filter_var($_POST["nombre"], FILTER_SANITIZE_STRING);
        $lname = filter_var($_POST["apellido"], FILTER_SANITIZE_STRING);
        $esp = filter_var($_POST["especialidad"], FILTER_SANITIZE_STRING);

        $query = "insert into profesor (nombre, apellido, especialidad, passw) values ($1,$2,$3,$4);";

        $result = pg_query_params($dbconn, $query, array($name,$lname,$esp,$passw));

        if (!$result){
            echo "<p>NACK</p>";
            pg_close($dbconn);
            header('Location:  ../registerProfesor.php?msg=baddb');
        }
        else{
            $idquery = "select max(idprofesor) from profesor"; //supone que el ultimo profesor en registrar es el recien ingresado (si 2 profesores registran a la vez, hay carreras criticas de datos)
            $idresult = pg_query($dbconn,$idquery);
            if(!$result){
                pg_close($dbconn);
                header('Location: ../registerProfesor.php?msg=baddb');
            }
            else{
                pg_close($dbconn);
                header('Location: ../registerProfesor.php?msg=okreg&newid='.pg_fetch_array($idresult)[0]);
            }
        }
    }
    else header('Location: ../index.php');
}
?>