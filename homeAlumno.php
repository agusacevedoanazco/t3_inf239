<?php
include 'alumnoNavBar.php'; 
$msg = (isset($_GET["msg"])) ? $_GET["msg"]: null;
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <?php
        if ($msg == "baddb") echo "<div class=\"alert alert-warning\">Error al conectarse a la base de datos</div>";
        elseif ($msg == "updok") echo "<div class=\"alert alert-success\">Alumno actualizado con éxito</div>";
    ?>
    <div class="jumbotron text-center">
        <h1>Actualizar Mis Datos</h1>
    </div>
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="updateController/updateAlumno.php" method="POST">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre">
                    </div>

                    <div class="form-group">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" name="apellido" id="apellido">
                    </div>

                    <div class="form-group">
                        <label for="anoIngreso">Año de Ingreso</label>
                        <input type="number" class="form-control" name="anoIngreso" id="anoIngreso">
                    </div>

                    <div class="form-group">
                        <label for="passw">Contraseña</label>
                        <input type="password" class="form-control" name="passw" id="passw" required>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                </form>
            </div>
        </div>
    </div>
</main>

</body>