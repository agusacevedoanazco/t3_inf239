CREATE TABLE profesor(
    idprofesor SERIAL PRIMARY KEY,
    nombre VARCHAR(45),
	apellido VARCHAR(45),
	especialidad VARCHAR(45),
    passw VARCHAR(400) NOT NULL
);

CREATE TABLE ramo(
    siglaramo VARCHAR(7) PRIMARY KEY,
    nombre VARCHAR(45),
    creditossct INT,
    semestre VARCHAR(6)
);

CREATE TABLE alumno(
    rolAlumno VARCHAR(15) PRIMARY KEY,
	nombre VARCHAR(45),
	apellido VARCHAR(45),
	anoingreso INT,
    passw VARCHAR(400) NOT NULL
);

CREATE TABLE mision(
	idmision SERIAL PRIMARY KEY,
	idprofesor INT REFERENCES profesor(idprofesor),
	idalumno VARCHAR(15) REFERENCES alumno(rolalumno),
	fechaingreso TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	descripcion VARCHAR(90),
	recompensa VARCHAR(60),
	estado SMALLINT DEFAULT 0
);

CREATE TABLE ayudantia(
    idayudantia VARCHAR(15) PRIMARY KEY,
    rolayudante VARCHAR(15) REFERENCES alumno(rolalumno),
    siglaramo VARCHAR(7) REFERENCES ramo(siglaramo),
    carga INT
);

CREATE TABLE asignacion(
    idayudantia VARCHAR(15) REFERENCES ayudantia(idayudantia),
    idmision INT REFERENCES mision(idmision),
    PRIMARY KEY (idayudantia,idmision)
);

CREATE TABLE imparticion(
    idprofesor INT REFERENCES profesor(idprofesor),
    siglaramo VARCHAR(7) REFERENCES ramo(siglaramo),
    PRIMARY KEY (idprofesor,siglaramo)
)