<?php
include 'profesorNavBar.php'; 
$msg = (isset($_GET["msg"])) ? $_GET["msg"]: null;
$newid = (isset($_GET["newid"])) ? $_GET["newid"] : null;
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <?php
        if ($msg == "baddb") echo "<div class=\"alert alert-warning\">Error al conectarse a la base de datos</div>";
        elseif ($msg == "okreg") echo "<div class=\"alert alert-success\">Profesor registrado exitosamente. Id del nuevo profesor: ".$newid."</div>";
    ?>
    <div class="jumbotron text-center">
        <h1>Registrar un nuevo Profesor</h1>
    </div>
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="registerController/newProfesor.php" method="POST">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" required>
                    </div>

                    <div class="form-group">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" name="apellido" id="apellido" required>
                    </div>

                    <div class="form-group">
                        <label for="especialidad">Especialidad</label>
                        <input type="text" class="form-control" name="especialidad" id="especialidad" required>
                    </div>

                    <div class="form-group">
                        <label for="passw">Contraseña</label>
                        <input type="password" class="form-control" name="passw" id="passw" required>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Registrar</button>
                </form>
            </div>
        </div>
    </div>
</main>

</body>