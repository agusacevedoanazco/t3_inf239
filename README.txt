# Tarea 3 Bases de Datos

## Tabla de contenidos

- [Tarea 3 Bases de Datos](#tarea-3-bases-de-datos)
  - [Tabla de contenidos](#tabla-de-contenidos)
  - [URL del Video](#url-del-video)
  - [Contenidos](#contenidos)
    - [Directorio Principal](#directorio-principal)
    - [Directorio "css"](#directorio-css)
    - [Directorio "registerController"](#directorio-registercontroller)
    - [Directorio "signInController"](#directorio-signincontroller)
    - [Directorio "updateController"](#directorio-updatecontroller)
  - [Consideraciones](#consideraciones)
  - [URL del Repositorio](#url-del-repositorio)
  - [Autores](#autores)

## URL del Video

## Contenidos

Respecto a la organizacion del proyecto, se puede separar en las siguientes divisiones.

### Directorio Principal

Contiene los archivos correspondientes a la interfaz web, los archivos de configuracion y conexion a la base de datos (dbtables.sql y dbconfig_og.php), el respaldo de la base de datos (dump.sql) y archivos correspondientes al respositorio (.gitignore)

### Directorio "css"

Se encuentran los archivos css correspondientes a los "estilos" de los componentes segun los nombres de los archivos, es decir, _navbars.css_ se encarga de otorgar las configuraciones esteticas de las barras de navegacion y _signin.css_ se encarga de otorgar las configuraciones esteticas de la pagina inicial de inicio de sesión.

### Directorio "registerController"

Se encuentran los archivos que insertan datos a las tablas. Es por ello que:

- assignMision.php: Inserta una nueva asociacion Mision - Ayudantia
- newAyudante.php: Inserta un nuevo ayudante
- newMision.php: Inserta una nueva Mision.
- newProfesor.php: Inserta un nuevo Profesor.

### Directorio "signInController"

Se encuentran los archivos relacionados al inicio de sesión y a la validacion de inicio de sesión, es por ello que los archivos se encargan de:

- signInAlumno.php: Inicia sesión con credenciales de alumno.
- singInProfesor.php: Inicia sesión con credenciales de Profesor
- signOut.php: Cierra la sesión activa
- signUpAlumno.php: Crea un nuevo alumno. Se decidió almacenar en este directorio y no en [registerController](#directorio-registercontroller), dado que la interfaz que gobierna la creación del alumno, es la dedicada a iniciar y validar las sesiones de usuario.
- validateAlumno.php:  Valida la sesión de alumno activa e identifica si el alumno es del tipo ayudante o no.
- validateProfesor.php: Valida la sesión de profesor activa.

### Directorio "updateController"

Se encuentran los archivos relacionados a la actualizacion de datos de las tablas de datos y/o eliminarlas.

- deleteAyudante.php: Revoca las facultades de un ayudante.
- toggleMision.php: Cambia el estado de la mision seleccionada.
- updateAlumno.php: Actualiza los datos del alumno que lo solicita.
- updateAlumno2.php: Actualiza los datos del alumno a partir de un identificado de alumno correctamente ingresado.
- updateMision.php: Actualiza los datos de una mision.
- updateProfesor.php: Actualiza los datos de un profesor.

## Consideraciones

- Se considera que un alumno/ayudante puede unicamente dictar una unica ayudantia.
- Se considera que un profesor dicta una única asignatura/ramo.

## URL del Repositorio

Los contenidos del directorio principal se encuentran en el link siguiente: [Repositorio](https://bitbucket.org/agusacevedoanazco/t3_inf239/src)

## Autores
