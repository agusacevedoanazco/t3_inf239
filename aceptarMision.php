<?php
    include 'alumnoNavBar.php';
    $msg = (isset($_GET["msg"])) ? $_GET["msg"] : null;
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <?php
    if ($msg == "baddb") echo "<div class=\"alert alert-danger\">Error al aceptar la mision</div>";
    elseif ($msg == "succ") echo "<div class=\"alert alert-success\">Mision Aceptada Correctamente</div>";
    elseif ($msg == "denied") echo "<div class=\"alert alert-warning\">No se puede asignar una mision destinada a si mismo!</div>";
    elseif ($msg == "nullay") echo "<div class=\"alert alert-warning\">Su id de ayudantia no está definido</div>";
    ?>

    <div class="jumbotron text-center">
        <h1>Aceptar Mision</h1>
    </div>
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="registerController/assignMision.php" method="POST">
                    <div class="form-group">
                        <label for="idmision">Id de Mision</label>
                        <input type="number" class="form-control" name="idmision" id="idmision" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Aceptar Mision</button>
                </form>
            </div>
        </div>
    </div>
</main>
</body>
